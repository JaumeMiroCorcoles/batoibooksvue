import { createRouter, createWebHistory } from 'vue-router'
import BooksList from '../views/BooksList.vue'
import BookForm from '../views/BookForm.vue'
import AppAbout from '../views/AppAbout.vue'
import AppCart from '../views/AppCart.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/list',
      name: 'list',
      component: BooksList
    },
    {
      path: '/form',
      name: 'form',
      component: BookForm
    },
    {
      path: '/about',
      name: 'about',
      component: AppAbout
    },
    {
      path: '/cart',
      name: 'cart',
      component: AppCart
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: BookForm
    }
  ]
})

export default router
