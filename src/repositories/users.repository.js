const urlData = import.meta.env.VITE_URL_API + '/users'
export default class UsersRepository {
  async getAllUsers() {
    const response = await fetch(urlData)
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async getUserById(id) {
    const response = await fetch(urlData + '/' + id)
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async addUser(user) {
    const response = await fetch(urlData, {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async removeUser(id) {
    const response = await fetch(urlData + '/' + id, {
      method: 'DELETE'
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async changeUser(user) {
    const response = await fetch(urlData + '/' + user.id, {
      method: 'PUT',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async updateUserPassword(id, contraseña) {
    const response = await fetch(urlData + '/' + id, {
      method: 'PATCH',
      body: JSON.stringify({ password: contraseña }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }
}
