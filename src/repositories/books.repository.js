const urlData = import.meta.env.VITE_URL_API + '/books'
export default class BooksRepository {
  async getAllBooks() {
    const response = await fetch(urlData)
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async getBookById(id) {
    const response = await fetch(urlData + '/' + id)
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async addBook(book) {
    const response = await fetch(urlData, {
      method: 'POST',
      body: JSON.stringify(book),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async removeBook(id) {
    const response = await fetch(urlData + '/' + id, {
      method: 'DELETE'
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async changeBook(book) {
    const response = await fetch(urlData + '/' + book.id, {
      method: 'PUT',
      body: JSON.stringify(book),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async updatePriceOfBook(id, priceLibro) {
    const response = await fetch(urlData + '/' + id, {
      method: 'PATCH',
      body: JSON.stringify({ price: priceLibro }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async existeLibro(idModule, idUser) {
    const response = await fetch(urlData + '/?idUser=' + idUser + '&idModule=' + idModule)
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json()
    return myData.length > 0
  }
}
