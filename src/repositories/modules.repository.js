const urlData = import.meta.env.VITE_URL_API + '/modules'
export default class ModulesRepository {
  async getAllModules() {
    const response = await fetch(urlData)
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async getModuleById(id) {
    const response = await fetch(urlData + '/' + id)
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async addModule(module) {
    const response = await fetch(urlData, {
      method: 'POST',
      body: JSON.stringify(module),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async removeModule(code) {
    const response = await fetch(urlData + '/' + code, {
      method: 'DELETE'
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }

  async changeModule(module) {
    const response = await fetch(urlData + '/' + module.code, {
      method: 'PUT',
      body: JSON.stringify(module),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (!response.ok) {
      throw `Error ${response.status} de la BBDD: ${response.statusText}`
    }
    const myData = await response.json() // recordad que .json() tb es una promesa
    return myData
  }
}
