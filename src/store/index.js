import { reactive } from 'vue'

export default {
  debug: true,
  state: reactive({
    messages: []
  }),
  delMessage(id) {
    const posicion = this.state.messages.findIndex((message) => message.id === id)
    this.state.messages.splice(posicion, 1)
  },
  addMessage(message) {
    this.state.messages.push({
      id: ++id,
      text: message
    })
  }
}
let id = 1
